<?php

include('./_head.php'); // include header markup ?>

<div class="col-sm-9 col-12">
	<div class="main">
		<div class="info">
	
	<?php 
	
		// output bodycopy
		echo $page->body; 
	
		
		// TIP: Notice that this <div id='content'> section is
		// identical between home.php and basic-page.php. You may
		// want to move this to a separate file, like _content.php
		// and then include('./_content.php'); here instead, on both
		// the home.php and basic-page.php template files. Then when
		// you make yet more templates that need the same thing, you
		// can simply include() it from them.
	
	?>
		</div>
	</div>
</div>


<?php include('./_foot.php'); // include footer markup ?>
