<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $page->title; ?></title>
	<meta name="description" content="<?php echo $page->summary; ?>" />
	<link href='//fonts.googleapis.com/css?family=Lusitana:400,700|Quattrocento:400,700' rel='stylesheet' type='text/css' />
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" /> -->
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/nrada.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>

<div class="container">
	<div class="row">
			<div class="toolbar row">
					<span class="toolbar-element col-md-7 col-12">
						Приєднуємося до нас в соцмережах!
					</span>
					<div class="col-md col-12">
						<a href="https://www.facebook.com/groups/1148516441992751/" target="__blank" class="toolbar-element">
							<img src="<?php echo $config->urls->templates?>img/facebook.png" alt="" class="toolbar-icon">
						</a>
						<a href="https://t.me/lugazakon" target="__blank" class="toolbar-element">
							<img src="<?php echo $config->urls->templates?>img/telegram.png" alt="" class="toolbar-icon">
						</a>
						<a href="https://instagram.com/narodnarada?utm_source=ig_profile_share&igshid=hgh054d8bl5u" target="__blank" class="toolbar-element">
							<img src="<?php echo $config->urls->templates?>img/instagram.png" alt="" class="toolbar-icon">
						</a>
						<a href="https://twitter.com/5Fmhn6ofAboVbno?s=09" target="__blank" class="toolbar-element">
							<img src="<?php echo $config->urls->templates?>img/twitter.png" alt="" class="toolbar-icon">
						</a>
						<a href="https://www.youtube.com/watch?v=9BTX71nihBA&feature=youtu.be" target="__blank" class="toolbar-element">
							<img src="<?php echo $config->urls->templates?>img/youtube.png" alt="" class="toolbar-icon">
						</a>
					</div>
				</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-12 sidebar">

		<img src="<?php echo $config->urls->templates?>img/logo.png" class="logo">

		<?php
			// top navigation consists of homepage and its visible children
			$homepage = $pages->get('/'); 
			$children = $homepage->children();

			// make 'home' the first item in the navigation
			$children->prepend($homepage); 

			// render an <li> for each top navigation item
			foreach($children as $child) {
				if($child->id == $page->rootParent->id) {
					// this $child page is currently being viewed (or one of it's children/descendents)
					// so we highlight it as the current page in the navigation
					echo "<div class='sidebar-info'><a href='$child->url'>$child->title</a></div>";
				} else {
					echo "<div class='sidebar-info'><a href='$child->url'>$child->title</a></div>";
				}
			}

			?>

		</div>
	


