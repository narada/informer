# Requirements

You must have `docker` and `docker-compose` installed.

# Local Install

- $ `docker-compose up` - Create and run docker containres (it can take up to 5 minutes)
- go to `http:localhost:8080` and run installation
- for mysql setup use next credentials (they are defined in ./docker-compose.yml):
```
host: mysqldb
database: nrada
user: user
password: pass
```

